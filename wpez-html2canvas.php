<?php
/**
 * Plugin Name: WPezPlugins: html2canvas
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-html2canvas
 * Description: Converts a DOM element to canvas (which can be saved as a PNG). See README for more details.
 * Version: 0.0.0
 * Author: MF Simchock (Chief Executive Alchemist) at Alchemy United
 * Author URI: https://AlchemyUnited.com
 * License: GPLv2 or later
 * Text Domain: wpez-h2c
 *
 * @package WPezH2c
 */

namespace WPezH2C;

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueueScripts' );

function enqueueScripts(){

	// https://html2canvas.hertzen.com/?ref=hackernoon.com
	\wp_enqueue_script(
		'html2canvas',
		plugin_dir_url( __FILE__ ) . '/assets/dist/js/html2canvas.min.js',
		array(),
		'1.3.2',
		false,
	);
}

function h2cSelect(){

	// We'll use a select to allow for multiple style "outputs"

	?>

	<div id="wpez-h2c-select-wrap">
		<label for="wpez-h2c-select">WPezH2C</label>
		<select id="wpez-h2c-select" name="title">
			<option value="" selected></option>
			<option value="sqr">Square</option>
		</select>
	</div>

	<?php 
}

/**
 * Add the custom styles to the page.
 *
 * @return
 */
function h2cStyle() {

	?>

<style>

.has-scrolled #wpez-h2c-select-wrap{
	display: none;
}

#wpez-h2c-select-wrap{
	position: fixed;
	left: 1.0rem;
	bottom: 1.0rem;
}

#wpez-h2c-select-wrap:hover{
	opacity: .25;
}

#wpez-h2c-select-wrap label,
#wpez-h2c-select-wrap select{
	display: block;
}

#wpez-h2c-sqr{
	background: black;
	padding: 5vh;
	width: 100vh;
	height: 100vh;
	display: flex;
	justify-content: center;
	align-items: center;
	position: relative;
	border: 2px solid #ffffff;
}

#wpez-h2c-sqr .entry-title{
	font-size: 4.0rem
}

#wpez-h2c-sqr .entry-excerpt{
	font-size: 2.0rem;
}

#wpez-h2c-sqr .wpez-h2c-branding{
	position: absolute;
	bottom: 1rem;
	right: 2rem;
	font-size: 2.0rem;
	font-family: "Courier New",Courier,monospace;
	font-weight: 700;
}

</style>    
	<?php
}

/**
 * Add the JS
 *
 * @param boolean|string $branding Pass the branding string, or not.
 *
 * @return void
 */
function h2cScript( $branding = false ) {

	?>

<script>
	document.addEventListener("DOMContentLoaded", function() {
		// get the h2c select
		const h2cSelect = document.querySelector('#wpez-h2c-select-wrap select');

		if (h2cSelect === null){
			return;
		}
		// listen for a change to the select
		h2cSelect.addEventListener("change", function(){

			let value = h2cSelect.options[h2cSelect.selectedIndex].value;
			if (value === ''){
				// change to ""? reload the page.
				window.location.reload();
				return;
			}
			// get the element to be restyled
			var eleH2C = document.querySelector("#wpez-h2c");
			// HTML 2 Canvas - has issue with some CSS properties, so strip the class (to start from scratch) and then use custom styles (above) that are H2C -friendly as needed.
			eleH2C.removeAttribute("class");
			<?php
			// Add a div for branding?
			if ( ( is_string( $branding ) ) ) {
				?>
				elemAddDiv = document.createElement('div');
				elemAddDiv.classList.add('wpez-h2c-branding');
				elemAddDiv.innerHTML = "<?php echo esc_attr( $branding ); ?>";
				eleH2C.appendChild(elemAddDiv);

				<?php
			}
			?>
			// Update the ID of the element using the button value with the button value to target the styling.
			eleH2C.id = 'wpez-h2c-' + value;

			// https://html2canvas.hertzen.com/configuration
			const h2cArgs = {
				// "windowWidth": '100%',
				// "windowHeight": '100%',
			};
			html2canvas(eleH2C, h2cArgs).then(canvas => {
			// once the canvas is created, add it here. Or where ever you want. 
				document.querySelector("#masthead").appendChild(canvas)
			});
		});
	});
</script>    

	<?php
}

add_action( 'wp_footer', __NAMESPACE__ . '\wpFooter' );

function wpFooter() {

	if ( false === currentUserCan() ) {
		return;
	}

	h2cSelect();
	h2cStyle();
	$str_branding = get_bloginfo( 'name' );
	h2cScript( $str_branding );
}

function currentUserCan() {
	
	if ( current_user_can( 'edit_pages') || current_user_can( 'edit_posts' ) ) {
		return true;
	}
	return false;
}